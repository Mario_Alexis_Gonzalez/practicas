#include "edo.h"

int Se_Edo(int car, int edo2){
    estados edosig;
    switch(edo2){
        //Casos para go_Norte
        case 0:
            switch(car){
                case 0:
                    edosig = goNorte;
                    break;
                case 1:
                    edosig = waitNorte;
                    break;
                case 2:
                    edosig = goNorte;
                    break;
                case 3:
                    edosig = waitNorte;
                    break;
            }
            break;
        //Caso para wait_Norte
        case 1:
            edosig = goEste;
            break;
        //casos para go_Este
        case 2:
            switch(car){
                case 0:
                    edosig = goEste;
                    break;
                case 1:
                    edosig = goEste;
                    break;
                case 2:
                    edosig = waitEste;
                    break;
                case 3:
                    edosig = waitEste;
                    break;
            }
            break;
        //caso para wait_Norte
        case 3:
            edosig = goNorte;
            break;
    }
    //actual=edosig;
    return edosig;
}

void Estados(int edo){
    switch(edo){
        case 0:
            printf("goNorte");
            break;
        case 1:
            printf("waitNorte");
            break;
        case 2:
            printf("goEste");
            break;
        case 3:
            printf("waitEste");
            break;
    }
}

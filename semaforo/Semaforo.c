#include <stdio.h>
#include <stdlib.h>

int actual;
void Estados(int edo);
void Se_Edo(int car, int edo2);

typedef enum{
    goNorte,
    waitNorte,
    goEste,
    waitEste
} estados;

typedef enum{
    No_Car,
    Car_Este,
    Car_Norte,
    Car_Ambos
} carros;

int main(){
    int es;
    int array[4]={No_Car, Car_Norte, Car_Este, Car_Ambos};
    estados edosig;
    edosig=2;
    actual=edosig;
    int cont;

    do{
        printf("\nActualmente esta en: ");
        Estados(actual);
        Se_Edo(array[cont], actual);
        cont++;
    }while(cont<=4);
    return 0;
}

void Se_Edo(int car, int edo2){
    estados edosig;
    switch(edo2){
        //Casos para go_Norte
        case 0:
            switch(car){
                case 0:
                    edosig = goNorte;
                    break;
                case 1:
                    edosig = waitNorte;
                    break;
                case 2:
                    edosig = goNorte;
                    break;
                case 3:
                    edosig = waitNorte;
                    break;
            }
            break;
        //Caso para wait_Norte
        case 1:
            edosig = goEste;
            break;
        //casos para go_Este
        case 2:
            switch(car){
                case 0:
                    edosig = goEste;
                    break;
                case 1:
                    edosig = goEste;
                    break;
                case 2:
                    edosig = waitEste;
                    break;
                case 3:
                    edosig = waitEste;
                    break;
            }
            break;
        //caso para wait_Norte
        case 3:
            edosig = goNorte;
            break;
    }
    actual=edosig;
}

void Estados(int edo){
    switch(edo){
        case 0:
            printf("goNorte");
            break;
        case 1:
            printf("waitNorte");
            break;
        case 2:
            printf("goEste");
            break;
        case 3:
            printf("waitEste");
            break;
    }
}

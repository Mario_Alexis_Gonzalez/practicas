#include <stdio.h>

typedef struct video{
    char titulo [41];
    int visitas;
    float tiempo;

}video_t;


video_t crear_video(){
    
    video_t nuevo_video;

    
    printf("\nIngrese el nombre del video: ");
    fflush(stdin);
    gets(nuevo_video.titulo);

    
    printf("\nIngrese el numero de visitas: ");
    fflush(stdin);
    scanf(" %d",&nuevo_video.visitas);

   
    printf("\nIngrese el tiempo del video: ");
    fflush(stdin);
    scanf(" %f",&nuevo_video.tiempo);

    
    return nuevo_video;

}

void imprimir_video(video_t video){
    printf("%s , timepo: %g , visitas: %d \n",video.titulo,video.tiempo,video.visitas);
}

int main(void){
    
    video_t v1 = crear_video();
    video_t v2= crear_video();

    
    imprimir_video(v2);
    imprimir_video(v1);

    return 0;
}
